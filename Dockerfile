# Base image to build from
FROM openjdk:8-alpine

# Mantainer:
MAINTAINER UX Forms

# Some Environment Variables
ENV HOME /root
ENV SBT_VERSION 1.2.8

# Install support tools
RUN apk --update add ca-certificates wget tar bash

# Install SBT
RUN wget -qO - --no-check-certificate "https://piccolo.link/sbt-${SBT_VERSION}.tgz" | tar xz -C /usr/local --strip-components=1

# Install scala
RUN sbt sbtVersion

