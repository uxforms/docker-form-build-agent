# docker-form-build-agent

A docker image for building forms for [https://uxforms.com](UX Forms).

Tested and working in [BitBucket Pipelines](https://bitbucket.org/product/features/pipelines), but should be useful anywhere else too.

## Dockerhub

Available on [dockerhub](https://hub.docker.com/r/uxforms/form-build-agent).

## Local development

If you want to keep your dev machine clean you can also use this image locally to build and test forms.

### First time init

This is likely to take some time as sbt needs to download itself and a bunch of initial dependencies.

```
docker run --name my-form-build \
 -it \
 -v "$PWD":/usr/src/form \
 -w /usr/src/form \
 --user $(id -u):$(id -g) \
 -e JAVA_TOOL_OPTIONS="-Duser.home=/tmp/home" \
 uxforms/form-build-agent:latest sbt
```

(where `my-form-build` should reference the name of your form so the container can be easily distinguished from others for other form builds).

### Subsequent builds

But all this will be cached on subsequent runs:

`docker start -ai my-form-build`

And there we have it - an isolated docker container that you can re-use for building and testing forms locally without having to worry about which version of java you have installed, and without it filling up your own maven and ivy resolution caches.

